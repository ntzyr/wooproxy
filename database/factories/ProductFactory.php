<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    $product_title = $faker->words(random_int(1, 5));
    return [
        'name' => implode(" ", $product_title),
        'slug' => implode("-", $product_title),
        'status' => 'publish',
        'featured' => false,
        'description' => $faker->paragraph(random_int(1,4)),
        'short_description' => $faker->paragraph(random_int(1,2)),
        'sku' => $faker->numberBetween(1000,9999),
        'price' => $faker->numberBetween(1000,99999),
        'date_on_sale_from' => $faker->dateTime(),
        'date_on_sale_to' => $faker->dateTime(),
        'total_sales' => $faker->numberBetween(1000,99999),
        'tax_status' => 'no',
        'manage_stock' => false,
        'stock_status' => false,
        'backorders' => 'no',
        'sold_individually' => true,
        'weight' => 123,
        'length' => 345,
        'width' => 423,
        'height' => 3423,
        'reviews_allowed' => true,
        'virtual' => false,
        'downloadable' => false
    ];
});
