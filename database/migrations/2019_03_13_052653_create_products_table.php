<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->string('status');
            $table->boolean('featured');
            $table->longText('description');
            $table->longText('short_description');
            $table->string('sku');
            $table->decimal('price');
            $table->decimal('sale_price')->nullable();
            $table->dateTime('date_on_sale_from')->nullable();
            $table->dateTime('date_on_sale_to')->nullable();
            $table->integer('total_sales')->nullable();
            $table->string('tax_status');
            $table->string('tax_class')->nullable();
            $table->boolean('manage_stock');
            $table->integer('stock_quantity')->nullable();
            $table->boolean('stock_status');
            $table->string('backorders');
            $table->boolean('sold_individually');
            $table->decimal('weight');
            $table->decimal('length');
            $table->decimal('width');
            $table->decimal('height');
            $table->boolean('reviews_allowed');
            $table->boolean('virtual');
            $table->boolean('downloadable');
            $table->integer('download_limit')->nullable();
            $table->integer('download_expiry')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
